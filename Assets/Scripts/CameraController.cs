﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    /* we will drag the player GameObject into this property in the editor. */
    public GameObject player;
    private Vector3 cameraOffset;

	
	void Start () {
        /* get the offset */
        cameraOffset = transform.position - player.transform.position;
	}
	
	// Update is called once per frame
	void LateUpdate () {
        transform.position = player.transform.position + cameraOffset;
	}
}
