﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TriggerPad : MonoBehaviour
{
    private Text messageText;

    private void Start()
    {
        /* get the reference to the game object using it's name */
        messageText = GameObject.Find("MessageText").GetComponent<Text>();
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Pad triggered.");
        messageText.text = "Pad triggered.";

    }
}
