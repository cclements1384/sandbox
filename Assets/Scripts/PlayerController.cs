﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public float speed;
    private Rigidbody rb;

    public float jumpForce;
    private bool canJump;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }


    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Space))
        {
            canJump = true;
        }
    }


    private void FixedUpdate()
    {
        /* move left right and up and down. */
        var moveHorizontal = Input.GetAxis("Horizontal");
        var moveVertical = Input.GetAxis("Vertical");
        var movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        rb.AddForce(movement * speed);

        /* apply a jump */
        if (canJump)
        {
            canJump = false;
            rb.AddForce(0.0f, jumpForce, 0.0f, ForceMode.Impulse);
        }

    }
}
