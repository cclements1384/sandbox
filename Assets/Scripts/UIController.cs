﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {

    public string Message;

    private void Start()
    {
        GetComponent<Text>().text = Message;
    }

    private void Update()
    {
        /* no need to overwrite the text here. */
    }
}
